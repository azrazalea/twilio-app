require 'rails_helper'
require 'json'

RSpec.describe MessagesController, :type => :controller do
  describe 'GET #index' do
    it 'returns a JSON array' do
      get :index
      expect(JSON.parse(response.body)).to be_a Array
    end
  end

  describe 'GET #show' do
    context 'when ID is invalid' do
      it 'returns error' do
        get :show, id: 13948021843908
        expect(JSON.parse(response.body)).to have_key "error"
      end
    end

    context 'when ID is valid' do
      it 'returns message data' do
        message = generate_message(true)
        puts message.inspect
        get :show, id: message.id
        expect(JSON.parse(response.body)).to_not have_key "error"
        expect(JSON.parse(response.body)).to include(
          "body" => message.body,
          "title" => message.title
          )
      end
    end
  end

  describe "POST #create" do
    context 'data invalid' do
      it 'returns error' do
        post :create, body: generate_text # No title
        expect(JSON.parse(response.body)).to have_key "error"
      end
    end

    context 'data valid' do
      it "returns no error" do
        data = generate_message
        post :create, data.attributes
        expect(JSON.parse(response.body)).to_not have_key "error"
      end

      it "returns data properly" do
        data = generate_message
        post :create, data.attributes
        json_body = JSON.parse(response.body)
        expect(json_body).to include("body" => data.body, "title" => data.title, "secret" => data.secret)
      end

      it "saves data in database" do
        data = generate_message
        post :create, data.attributes
        json_body = JSON.parse(response.body)
        get :show, id: json_body["id"]
        json_body = JSON.parse(response.body)
        expect(json_body).to include("body" => data.body, "title" => data.title, "secret" => data.secret)
      end
    end
  end

  describe "PUT #update" do
    context 'when ID is invalid' do
      it 'returns error' do
        put :update, id: 13948021843908
        expect(JSON.parse(response.body)).to have_key "error"
      end
    end

    context 'when ID is valid' do
      context 'when data is invalid' do
        it 'returns error' do
          message = generate_message(true)
          message2 = generate_message(true)
          put :update, id: message.id, secret: message2.secret
          expect(JSON.parse(response.body)).to have_key "error"
        end
      end

      context 'when data is valid' do
        it 'returns no error' do
          data = generate_message(true)
          put :update, id: data.id, title: "blah"
          expect(JSON.parse(response.body)).to_not have_key "error"
        end

        it 'returns data properly' do
          data = generate_message(true)
          put :update, id: data.id, title: "blah"
          expect(JSON.parse(response.body)["title"]).to eq "blah"
        end

        it 'saves data in database' do
          data = generate_message(true)
          put :update, id: data.id, title: "blah"
          get :show, id: data.id
          expect(JSON.parse(response.body)["title"]).to eq "blah"
        end
      end
    end
  end

  describe "DELETE #destroy" do
    context 'when ID is invalid' do
      it 'returns error' do
        delete :destroy, id: 389124029384390281432908
        expect(JSON.parse(response.body)).to have_key "error"
      end
    end

    context 'when ID is valid' do
      it 'returns no error' do
        data = generate_message(true)
        delete :destroy, id: data.id
        expect(JSON.parse(response.body)).to_not have_key "error"
      end

      it 'deletes item from database' do
        data = generate_message(true)
        delete :destroy, id: data.id
        expect(Message.find_by_id(data.id)).to be_nil
      end
    end
  end
end
