require 'rails_helper'

RSpec.describe CallLogsController, :type => :controller do
  describe 'GET #index' do
    it 'returns a JSON array' do
      get :index
      expect(JSON.parse(response.body)).to be_a Array
    end
  end

  describe 'GET #show' do
    context 'when ID is invalid' do
      it 'returns error' do
        get :show, id: 13948021843908
        expect(JSON.parse(response.body)).to have_key "error"
      end
    end

    context 'when ID is valid' do
      it 'returns CallLog data' do
        call_log = generate_call_log(true)
        get :show, id: call_log.id
        expect(JSON.parse(response.body)).to_not have_key "error"
        expect(JSON.parse(response.body)).to include(
          "number" => call_log.number,
          "message_id" => call_log.message.id
          )
      end
    end
  end
end
