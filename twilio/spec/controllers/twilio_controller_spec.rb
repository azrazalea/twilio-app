require 'rails_helper'
require 'nokogiri'

RSpec.describe TwilioController, :type => :controller do
  describe "GET #index" do
    it "responds with a valid response" do
      get :index
      expect(response.code).to eq "200"
      expect(Nokogiri::XML(response.body).xpath("//Response")).to_not be_empty
    end
  end

  describe "GET #get_message" do
    context "valid message secret" do
      it "has a valid response" do
        message = generate_message(true)
        get :get_message, "Digits" => message.secret
        expect(response.code).to eq "200"
        expect(Nokogiri::XML(response.body).xpath("//Response")).to_not be_empty
      end

      it "contains the message body" do
        message = generate_message(true)
        get :get_message, "Digits" => message.secret
        expect(response.body).to include(message.body)
      end

      it "accepts message instead of Digits" do
        message = generate_message(true)
        get :get_message, "message" => message.secret, "Digits" => 3243209420948320
        expect(response.body).to include(message.body)
      end

      context "disabled message" do
        it "doesn't contain message body" do
          message = generate_message(true)
          message.disabled = true
          message.save
          get :get_message, "Digits" => message.secret
          expect(response.body).to_not include(message.body)
        end
      end
    end

    context "invalid message secret" do
      it "has a valid response" do
        get :get_message, "Digits" => 99934132432443
        expect(response.code).to eq "200"
        expect(Nokogiri::XML(response.body).xpath("//Response")).to_not be_empty
      end
    end
  end
end
