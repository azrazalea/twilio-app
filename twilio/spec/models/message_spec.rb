require 'rails_helper'

RSpec.describe Message, :type => :model do
  it "includes call_logs in json output" do
    message = generate_message(true)
    message.call_logs.create(number: 'blah')
    expect(message.as_json).to include("call_logs")
  end
end
