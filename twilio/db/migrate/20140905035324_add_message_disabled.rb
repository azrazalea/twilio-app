class AddMessageDisabled < ActiveRecord::Migration
  def change
    add_column :messages, :disabled, :boolean
  end
end
