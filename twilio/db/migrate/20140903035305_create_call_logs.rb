class CreateCallLogs < ActiveRecord::Migration
  def change
    create_table :call_logs do |t|
      t.text :number
      t.references :message, index: true

      t.timestamps
    end
  end
end
