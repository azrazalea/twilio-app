class CallLogsController < ApplicationController
  def index
    render json: CallLog.all
  end

  def show
    call_log = CallLog.find_by_id(params[:id])
    if call_log.nil?
      ret_message = {error: "Message not found with ID #{params[:id]}"}
    end

    render json: ret_message || call_log
  end
end
