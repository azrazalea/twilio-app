class TwilioController < ApplicationController
  def index
    msg = Twilio::TwiML::Response.new do |r|
      r.Say "Hello, welcome to Scavenger Hunt Express."
      r.Gather action: '/twilio/get_message', method: 'get' do |g|
        g.Say "Please enter the secret number of the message and end with #."
      end
    end.text

    render xml: msg
  end

  def get_message
    secret = params["message"]
    secret ||= params["Digits"]

    message_object = Message.enabled.secret(secret).first

    if message_object.nil?
      message = "No message with secret #{secret} found."
    else
      message = message_object.body
    end

    # This call is directly from Twilio, not from us
    # There is probably a better way to detect this
    # Basically, only want to log the beginning of calls not repeats
    if not params["message"]
      CallLog.create(
        number: params["From"],
        message: message_object
        )
    end


    msg = Twilio::TwiML::Response.new do |r|
      r.Say message

      r.Gather numDigits: 1, action: "/twilio/get_message?message=#{params['Digits']}", method: 'get' do |g|
        g.Say "Press any key to hear this message again."
      end
    end.text

    render xml: msg
  end
end
