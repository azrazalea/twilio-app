class MessagesController < ApplicationController
  def index
    render json: Message.all.order('updated_at DESC')
  end

  def show
    message = Message.find_by_id(params[:id])
    if message.nil?
      ret_message = {error: "Message not found with ID #{params[:id]}"}
    end

    render json: ret_message || message
  end

  def update
    message = Message.find_by_id(params[:id])
    if message.nil?
      ret_message = {error: "Message not found with ID #{params[:id]}"}
    end

    if message and not message.update(msg_params)
      ret_message = {error: "Unable to update message"}
    end

    render json: ret_message || message
  end
  alias :edit :update

  def create
    message = Message.create(msg_params)
    if message.id.nil?
      ret_message = {error: "Unable to create message"}
    end

    render json: ret_message || message
  end
  alias :new :create

  def destroy
    message = Message.find_by_id(params[:id])
    if message.nil?
      ret_message = {error: "Message not found with ID #{params[:id]}"}
    end

    if not message.nil? and not Message.destroy(message.id)
      ret_message = {error: "Unable to delete message #{message.id}"}
    end

    render json: ret_message || message
  end

  private
  def msg_params
    params.permit(:id, :body, :title, :secret, :disabled)
  end
end
