(function () {
  var app = angular.module('messages', ['restangular', 'templates']);

  app.controller('MessagesController', function($scope, Restangular){
    $scope.baseMessages = Restangular.all('messages');

    $scope.baseMessages.refresh = function(){
      $scope.baseMessages.getList().then(function(messages){
        $scope.messages = messages;
      });
    };

    $scope.baseMessages.remove = function(message){
      message.remove();
      $scope.baseMessages.refresh();
    };

    $scope.baseMessages.refresh();

  });

  app.directive('messageForms', function(){
    return {
      restrict: "E",
      templateUrl: 'message-forms.html'
    };
  });

  app.controller('MessageController', function($scope, $timeout){
    var timeout = null;
    var debounceUpdate = function(new_message, old_message){
      if(!new_message.id){
        if($scope['message__form'].$valid){
          if(timeout){
            $timeout.cancel(timeout);
          }
          timeout = $timeout(function(){
            $scope.baseMessages.post(new_message);
            $scope.baseMessages.refresh();
          }, 1000);
        }

        return;
      }

      if(new_message == old_message || $scope['message_' + new_message.id + '_form'].$invalid){
        return;
      }
      if(timeout) {
        $timeout.cancel(timeout);
      }
      timeout = $timeout(new_message.save, 1000);
    };
    $scope.$watchCollection('message', debounceUpdate);
  });
})();
