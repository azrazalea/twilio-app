class Message < ActiveRecord::Base
  validates :title, presence: true
  validates :body, presence: true
  validates :secret, presence: true, uniqueness: true
  before_save :default_values

  has_many :call_logs

  scope :enabled, -> {where disabled: false}
  scope :secret, -> (secret) {where secret: secret}

  def default_values
    self.disabled = false if self.disabled.nil?
    true
  end

  def as_json(options = {})
    super(include: :call_logs)
  end
end
